// vendor imports
import React from 'react';
// styles
import './style.css';
const Button = (props) => {
    return (
        <button className={`button button-${props.type}`} onClick={props.onClick}>
            {props.label}
        </button>
    );
};

export default Button;