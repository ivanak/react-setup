// vendor imports
import React from 'react';
// styles
import './style.css';

const Input = (props) => {
    return (
        <div className='input-group'>
            <span className='input-group__label'>{props.label}</span>
            <input
                autoComplete='off'
                className='input-group__input'
                placeholder={props.placeholder}
                type='text'
                name={props.name}
                value={props.value}
                onChange={props.onChange}
            />
        </div>
    );
};

export default Input;