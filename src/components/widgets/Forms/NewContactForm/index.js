// vendor imports
import React, { useState } from 'react';
import { connect } from 'react-redux';
// components
import Input from 'components/ui/Input';
import Button from 'components/ui/Button';
// actions
import { addContact } from 'redux/ducks/contacts';
// services
import { checkDuplicates } from 'services/functions';
import { addContactInLocalStorage } from 'services/localStorage';
// constants
import { CONTACT_ADDED, DUPLICATE_CONTACT } from 'data/const';
// styles
import './style.css';

const NewContactForm = (props) => {
    const [form, setForm] = useState({
        firstName: '',
        lastName: '',
        email: ''
    });
    const [title, setTitle] = useState('');

    const addMyContact = () => {
        let contact = {
            id: Math.random().toString(36).substr(2, 9),
            firstName: form.firstName,
            lastName: form.lastName,
            email: form.email
        };
        if (!checkDuplicates(contact.email, props.contacts)) {
            props.addContact(contact);
            addContactInLocalStorage(contact, props.contacts);
            setTitle(CONTACT_ADDED);
            resetForm();
        } else {
            setTitle(DUPLICATE_CONTACT);
        }
        resetTitle();
    };

    const resetForm = () => {
        setForm({
            firstName: '',
            lastName: '',
            email: ''
        });
    };

    const resetTitle = () => {
        setTimeout(() => {
            setTitle('');
        }, 3000);
    };

    const handleChange = (event) => {
        setForm(prevState => {
            return {
                ...prevState,
                [event.target.name]: event.target.value
            }
        });
    };

    return (
        <div className='form-wrapper'>
            <h1 className='title'>{title}</h1>
            <h1 className='title'>Your new contact</h1>
            <Input
                label='First name'
                placeholder='First name'
                name='firstName'
                value={form.firstName}
                onChange={handleChange}
            />
            <Input
                label='Last name'
                placeholder='Last name'
                name='lastName'
                value={form.lastName}
                onChange={handleChange}
            />
            <Input
                label='Email'
                placeholder='Email'
                name='email'
                value={form.email}
                onChange={handleChange}
            />
            <Button
                label='Add new contact'
                type='primary'
                onClick={addMyContact}
            />
        </div>
    );
};

const mapStateToProps = state => {
    return {
        contacts: state.contacts
    };
};

const mapDispatchToProps = dispatch => {
    return {
        addContact: (contact) => { dispatch(addContact(contact)) }
    }
};


export default connect(mapStateToProps, mapDispatchToProps)(NewContactForm);