// vendor imports
import React from 'react';
import { Link } from 'react-router-dom';
// components
import NewContactForm from 'components/widgets/Forms/NewContactForm';
// constants
import ROUTES from 'data/routes';
// styles
import './style.css';

const AddNewContactPage = () => {
    return (
        <>
            <NewContactForm />
            <Link to={ROUTES.ROOT}>See all contacts</Link>
        </>
    );
};

export default AddNewContactPage;