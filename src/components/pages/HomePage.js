// vendor imports
import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
// actions
import { setContacts } from 'redux/ducks/contacts';
// services
import { getLocalStorageContacts } from 'services/localStorage';
// styles
import './style.css';

const HomePage = (props) => {
    const [title, setTitle] = useState('');

    useEffect(() => {
        let contacts = getLocalStorageContacts();
        props.setContacts(contacts);
        if (contacts.length > 0) {
            setTitle('Your contacts');
        } else {
            setTitle('Contacts list is empty.');
        }
    }, []);

    return (
        <>
            <h1 className='title'>{title}</h1>
            <table>
                <tbody>
                    <tr>
                        <th>First name</th>
                        <th>Last name</th>
                        <th>Email</th>
                    </tr>
                    {
                        props.contacts.map(contact => {
                            let { id, firstName, lastName, email } = contact;
                            return (
                                <tr key={id}>
                                    <td>{firstName} </td>
                                    <td>{lastName}</td>
                                    <td>{email}</td>
                                </tr>
                            );
                        })
                    }
                </tbody>
            </table>
            <Link className='new-contact-link' to='/addContact'>You can add new contacts here</Link>
        </>
    );
};

const mapStateToProps = state => {
    return {
        contacts: state.contacts
    };
};

const mapDispatchToProps = dispatch => {
    return {
        setContacts: (contacts) => { dispatch(setContacts(contacts)) }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);