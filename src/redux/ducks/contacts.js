// initial state
const init = {
    contacts: []
};

// constants
const SET_CONTACTS = 'SET_CONTACTS';
const ADD_CONTACT = 'ADD_CONTACT';

// actions
export const setContacts = (contacts) => {
    return {
        type: SET_CONTACTS,
        payload: contacts
    };
};

export const addContact = (contact) => {
    return {
        type: ADD_CONTACT,
        payload: contact
    };
};

// reducer
const reducer = (state = init, action) => {
    switch (action.type) {
        case SET_CONTACTS:
            return {
                ...state, contacts: action.payload
            }
        case ADD_CONTACT:
            return {
                ...state, contacts: [...state.contacts, action.payload]
            }
        default:
            return state;
    };
};

export default reducer; 