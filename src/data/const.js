export const CONTACT_ADDED = 'Contact successfully added.';
export const DUPLICATE_CONTACT = 'Contact with that email address already exists.';