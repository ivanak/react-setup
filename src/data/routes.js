const ROOT = '/';
const ADD_CONTACT = '/addContact';

let routes = {
	ROOT,
	ADD_CONTACT
};

export default routes;
