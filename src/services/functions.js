import { FLOWBASEANNOTATION_TYPES } from "@babel/types";

export const checkDuplicates = (email, contacts) => {
    let duplicateContacts = contacts.filter(contact => contact.email === email);
    return duplicateContacts.length > 0 ? true : false;
};