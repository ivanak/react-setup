
export const getLocalStorageContacts = () => {
    let contacts = localStorage.getItem('contacts');
    if (contacts) {
        return JSON.parse(contacts);
    } else {
        return [];
    }
};

export const setLocalStorageContacts = (contacts) => {
    localStorage.setItem('contacts', JSON.stringify(contacts));
};

export const addContactInLocalStorage = (contact, contacts) => {
    contacts.push(contact);
    setLocalStorageContacts(contacts);
};